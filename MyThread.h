/*
 * MyThread.h
 *
 *  Created on: 8 Jan 2014
 *      Author: rafles01
 */

#ifndef MYTHREAD_H_
#define MYTHREAD_H_

#include <ting/net/TCPSocket.hpp>
#include <ting/mt/Thread.hpp>
#include <plugin/Debug.h>

#include <iostream>
#include <string>

#include "MyTime.h"
#include "QueryRecData.h"

#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>

#define BUFFER_SIZE 4096*100
#define TIMEOUT  5
#define ILE_DEFAULT  3
const string WWWDIR = "www";
const string indexes[ILE_DEFAULT] = {"/index.html","/index.htm","/index.php"};

#if M_OS == M_OS_WINDOWS
#define PATH_MAX  4096
#endif

using namespace std;
using namespace ting::net;

class MyThread : public ting::mt::Thread{

private:
  ting::net::TCPSocket sock;

public:
  bool doCycle;

  MyThread(ting::net::TCPSocket& so) {
	  sock = so;
	  doCycle = true;
  }

  void  doResponseRequest(QueryRecData pars) {
	/*
	 * GET URL
	 */
	string url = pars.getUrl();
	if(!pars.fileOrDirExist((WWWDIR).c_str())) {
		Debug::co() << WWWDIR << " - directory does not exist" << Debug::endl();
		return ;
	}

	if(pars.isDir((WWWDIR+url).c_str())) {
		for(unsigned int i=0;i<ILE_DEFAULT;i++) {
			if(pars.fileExist(WWWDIR+url+indexes[i])) {
				url += indexes[i];
				break;
			}
		}
	}

	if(pars.fileExist(WWWDIR+url)) {
		Debug::co(sock) << url << Debug::endl();
		/*
		 *  if true = 304 code will be returned to browser
		 */
		bool cache;
		string str = pars.setHeader(WWWDIR+url,cache);

		   if(pars.getFileExtention(url)=="php") {
			   FILE * fp ;
			   string tstCommand ="/usr/bin/php5-cgi -q "+WWWDIR+url+" "+pars.getGET() ;
			   char path[PATH_MAX];
			   fp = popen(tstCommand.c_str(), "r");
			   string tbuf;
			   while ( fgets(path,PATH_MAX, fp ) != NULL ){
				   tbuf+=path;
			   	   }
			   pclose(fp);
			   std::size_t fileSize = tbuf.size();

			   ostringstream ss;
			   ss << fileSize;

			   str +="Content-Length: "+ss.str()+"\n";
			   str +="\n";
			   str +=tbuf;

			   //send header
			   ting::Buffer<ting::u8> data3((ting::u8*)str.c_str(),str.size());
			   size_t x=0;
			   while(x < fileSize) {
					x+=sock.Send(data3,x);
					usleep(1);
					}
			} else {
				ting::Buffer<ting::u8> data2((ting::u8*)str.c_str(),str.size());
				sock.Send(data2);
				if(!cache) {
					std::size_t fileSize = pars.getFileSize(WWWDIR+url);
					char* buff =pars.getData(WWWDIR+url);
					ting::Buffer<ting::u8> data3((ting::u8*)buff,fileSize);
					std::size_t x=0;
					while(x < fileSize) {
						x +=sock.Send(data3,x);
						usleep(1);
						}
				}
			}
		} else {
			bool cache;// if true = 304 code will be returned to browser
			string str=pars.setHeader(WWWDIR+url,cache);
			str+="<html><body><h1>404</h1></body></html>";
			ting::Buffer<ting::u8> data2((ting::u8*)str.c_str(),str.size());
			sock.Send(data2);
			Debug::co() << " Error 404 " << url << Debug::endl();
		}
  }
void Run() {
		try {
			ting::Array<ting::u8> data(BUFFER_SIZE);

			unsigned bRec2 = 0;
			unsigned stan  = 0;

			MyTime czasPob;
			string frame_content = "";

			while(true){
				stan = sock.Recv(data);
				bRec2 +=stan;

				frame_content = (char*)data.Begin();

				if(frame_content.find("\n\n")>0 && bRec2>0 ) {
					 break;
				}

				if(czasPob.upTime()>TIMEOUT) {
					Debug::co(sock) << czasPob.getAcTime() << " : Recive Timeout " << TIMEOUT << "s " << Debug::endl();
					doCycle=false;
					return;
				}
			}

			frame_content = (char*)data.Begin();
			QueryRecData pars(frame_content);

			if(pars.reqIsValid()) {
				doResponseRequest(pars);

				if(pars.getConnectionType() != "keep-alive") {
						if(sock.CanRead()) {
						   sock.Close();
						}
					}
			  }
		data.Reset();
		} catch(exception &e) {
			Debug::co() << "Error:" << e.what() << Debug::endl();
			doCycle=false;
		} catch(ting::net::Exc &e){
			Debug::co() << "Network error: " << e.What() << Debug::endl();
		}
		doCycle=false;
		Debug::co(sock) << "- thread closed" << Debug::endl();
	 }
};
#endif /* MYTHREAD_H_ */
