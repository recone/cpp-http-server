/*
 * QuaryRecData.h
 *
 *  Created on: 13 Jan 2014
 *      Author: Rafal Lesniewski
 */

#ifndef QUERYRECDATA_H_
#define QUERYRECDATA_H_

#include <vector>
#include <iostream>
#include <string>
#include <strstream>
#include <sstream>
#include <algorithm>
#include <fstream>

#include <sys/stat.h>
#include <sys/types.h>

#include <time.h>
#include "MyTime.h"

#define SENAME "Http_Recone"
const unsigned  int MAXITEMS = 43;
const string whiteSpaces = " \f\n\r\t\v";

using namespace std;

class QueryRecData {
private:
	vector<vector<string> > tree;
	string getParm = "";
	string mime1[MAXITEMS] ={
			"html","htm","shtml","php","xhtml",
			"css",
			"xml","rss",
			"gif",
			"jpeg","jpg",
			"js",

			"txt","ini",

			"png",
			"tif","tiff",
			"ico",
			"bmp",
			"svg","svgz",

			"json",
			"doc",
			"pdf",

			"ps","eps","ai",
			"rtf",
			"xls",

			"7z",

			"rar",
			"swf",
			"zip",

			"3gpp","3gp",
			"mp4",
			"mpeg","mpg","mpe","avi",
			"ogv",
			"mov",
			"webm"
	};
	string mime2[MAXITEMS] ={
			"text/html","text/html","text/html","text/html","text/html",
			"text/css",
			"text/xml ","text/xml",
			"image/gif",
			"image/jpeg","image/jpeg",
			"application/x-javascript",

			"text/plain","text/plain",

			"image/png",
			"image/tiff","image/tiff",
			"image/x-icon",
			"image/x-ms-bmp",
			"image/svg+xml","image/svg+xml",

			"application/json",
			"application/msword",
			"application/pdf",

			"application/postscript","application/postscript","application/postscript",
			"application/rtf",
			"application/vnd.ms-excel",

			"application/x-7z-compressed",

			"application/x-rar-compressed",
			"application/x-shockwave-flash",
			"application/zip",

			"video/3gpp","video/3gpp",
			"video/mp4",
			"video/mpeg","video/mpeg","video/mpeg","video/mpeg",
			"video/ogg",
			"video/quicktime",
			"video/webm",
	};
public:

/* =============== basics =============*/
	void trimRight( std::string& str,  const std::string& trimChars = whiteSpaces )
	{
	   std::string::size_type pos = str.find_last_not_of( trimChars );
	   str.erase( pos + 1 );
	}


	void trimLeft( std::string& str,  const std::string& trimChars = whiteSpaces )
	{
	   std::string::size_type pos = str.find_first_not_of( trimChars );
	   str.erase( 0, pos );
	}


	void trim( std::string& str, const std::string& trimChars = whiteSpaces )
	{
	   trimRight( str, trimChars );
	   trimLeft( str, trimChars );
	}
	void str_replace( string &s, const string &search, const string &replace )
	{
	  for(size_t pos = 0; ; pos += replace.length())
	 	{
	         pos = s.find( search, pos );
	         if( pos == string::npos ) break;

	         s.erase( pos, search.length() );
	         s.insert( pos, replace );
	    }
	}

/* ==================================== */

	void ReplaceStringInPlace(std::string& subject, const std::string& search,const std::string& replace) {
		std::size_t pos = 0;
	    while ((pos = subject.find(search, pos)) != std::string::npos) {
	         subject.replace(pos, search.length(), replace);
	         pos += replace.length();
	    }
	}
/*
 * Creates vector from added string - header HTTP request
 */
	QueryRecData(string st){

		/*
		* Validate request CHARS
		* acceptable char <= 127
		*/
		for(unsigned int i=0;i<st.size();i++) {
			if(st.at(i)>127) {
			   return;
			   }
		   }

		vector<string> listRows = QueryRecData::splitText(st);
		for(unsigned int ile=0;ile<listRows.size();ile++) {
			if(ile==0) {
				string txt = listRows.at(ile);
				trim(txt);
				tree.push_back(explodeSuper(txt,' '));
			 } else {
				tree.push_back(QueryRecData::explode(listRows.at(ile),':'));
			 }
		}
	}

	string printContent() {
		string buff = "";
		for(unsigned int ile=0;ile<tree.size();ile++) {
			string *list = tree.at(ile).data();
			buff += "|"+list[0]+ ":"+list[1]+"| \n";
		}
		return buff;
	}

	bool reqIsValid() {
		if( tree.size()>0) {
			vector<string> last = tree.back();
			string l = last.at(0).data();
			string r = last.at(1).data();

			if(r.size()==0 && l.size()==0)
				return true;
			}

		return false;
	}
	string getConnectionType() {
		for(unsigned int ile=0;ile<tree.size();ile++) {
			string *list = tree.at(ile).data();
			if(list[0]=="Connection") {
				return list[1];
			}
		}
		return " ";
	}

	string getIfModyfied() {
		for(unsigned int ile=0;ile<tree.size();ile++) {
			string *list = tree.at(ile).data();
			if(list[0]=="If-Modified-Since") {
				return list[1];
			}
		}
		return " ";
	}

	string getType(string ext) {
		for(unsigned int i=0;i<MAXITEMS;i++) {
				if(mime1[i]==ext) {
				  return mime2[i];
				}
		}
	return "application/octet-stream";
	}

	/*
	 *  Creates header
	 */
	string setHeader(string url,bool &cached) {
		cached  = false; // file is cached by browser?
		bool ex = false; // file exists
		string ext;

		// do file exists?
		ex = fileExist(url);

		string str ="";
		if(ex) {
			ext = getFileExtention(url); // get file extension

			if(getIfModyfied()==getFileModyfication(url) && ext!="php") {
					cached = true;
					str  ="HTTP/1.0 304 OK\n";
				} else
					str  ="HTTP/1.0 200 OK\n";

			MyTime ti;
			string ta = ti.getGMTime(ti.getAcTimeStamp());

			str +="Date: "+ta+"\n";
			str +="Content-Type: "+getType(ext)+" \n";

			  if(ext!="php") {
				str +="Keep-Alive: timeout=5, max=98 \n";
				str +="Cache-Control: max-age=315360000, public\n";
				str +="Expires: Thu, 31 Dec 2037 23:55:55 GMT\n";

				std::size_t size=0;
				size=getFileSize(url);
				ostringstream ss;
				ss << size;
				if(!cached)
					str +="Content-Length: "+ss.str()+"\n";
				str +="Last-Modified:"+getFileModyfication(url)+"\n";
				}

			} else {
				str  ="HTTP/1.0 404 OK\n";
				str +="Cache-Control: no-cache\n";
				str +="Content-Type: text/html; charset=UTF-8 \n";

			}
		str +="Set-Cookie: ci_session=12312312; expires=Sat, 09-Feb-2019 15:39:30 GMT; Max-Age=86400; path=asdasd\n";
		str +="Connection: Keep-Alive\n"; // Keep-Alive Close
		str +="Server: " SENAME "\n";
		str+="\n";

		return str;
	}

	/*
	 * Checks if this points to file
	 */
	bool isFile(const char* path) {
	    struct stat buf;
	    stat(path, &buf);
	    return S_ISREG(buf.st_mode);
	}
	/*
	 * Is it a directory ?
	 */
	bool isDir(const char* path) {
		try {
			if(isFile(path))
				return false;
			else
				return true;
		} catch (exception e) {
			return false;
		}
	}

	/*
	 * Checks if file exists
	 * if directory returns false
	 */
	bool fileExist(string name ){
		if(!isFile(name.c_str())) return false;
		ifstream f(name.c_str());
		if (f.good()) {
			f.close();
			return true;
		} else {
			f.close();
			return false;
		}
	}

	/*
	* checks if file or directory exists
	*/
	bool fileOrDirExist(string name ){
		struct stat info;
		if( stat( name.c_str(), &info ) != 0 )
			 return false;
		else if( info.st_mode & S_IFDIR )  // S_ISDIR() doesn't exist on my windows
			return true;
		else
			return false;
	}

	/*
	 * Returns data read from local file
	 */
	char* getData(string st) {
	std::size_t size;
	  ifstream file(st.c_str(),ios::in|ios::binary|ios::ate);
	  if (file.is_open())  {
		size = file.tellg();
		char * memblock = new char [size];
		file.seekg (0, ios::beg);
		file.read (memblock, size);
		file.close();
		return memblock;
	  }
	  return 0;
	}

	/*
	 * Gets local file size
	 */
	std::size_t getFileSize(string st) {
     ifstream file(st.c_str(),ios::in|ios::binary|ios::ate);
      if (file.is_open())  {
    	 return file.tellg();
      	 }
    return -1;
    }

	/*
	 * Gets URL send in request
	 */
	string getUrl() {
		for(unsigned int ile=0;ile<tree.size();ile++) {
			string* list = tree[ile].data();
			if(list[0]=="GET") {
				vector<string> buf = QueryRecData::explode(list[1],'?');
				if(buf.size()>1) {
					getParm = buf.at(1);
					}
				return buf.at(0);
			}
		}
		return "";
	}

	string getGET() {
		str_replace(getParm,"&"," ");
		return getParm;
	}

	/*
	 * Gets the file extension
	 */
	string getFileExtention(string file) {
	string buf="";
	bool dot=false;
	for(unsigned int i = 0; i <file.length(); i++ ) {
		   if( file[i] == '.' ) {
			   dot=true; buf="";
		   } else {
			   if(dot==true) {
				   buf+=file[i];
			   }
		   }
	   }
	return buf;
	}

	/*
	 * Converts string to list of rows
	 */
	vector<string> splitText(string st) {
		vector<string> list;
		std::istrstream in(st.c_str());
		string line;
		string::size_type pos = 0;
		while(std::getline(in,line)){
			replace(line.begin(), line.end(),'\r',' ');
			replace(line.begin(), line.end(),'\n',' ');
			list.push_back(line);
		}
		return list;
	}

	/*
	 * equal to php explode
	 */
	vector<string> explodeSuper(string st,char pr) {
		vector<string> list;
		list.clear();
		list.resize(0);
		int start =0, krok =0;
		for(unsigned int x=0;x<st.length();x++) {
			if(st.at(x)==pr){
				string buf = st.substr(start,krok);
				trim(buf);
				list.push_back(buf);
				start=x;
				krok=0;
			}
			krok++;
		}
		return list;
	}

	/*
	 * Brakes string on parts, creates vector of small strings
	 */
	vector<string> explode(string st, char pr) {
		vector<string> list;
		list.clear();
		list.resize(0);
		string line1="",line2 ="";
		bool sw=false;
		for(unsigned int x=0;x<st.length();x++) {
			if(st.at(x)==pr && sw==false) {
					sw=true;
				} else {
					if(sw==false) {
						line1+=st.at(x);
					} else {
						line2+=st.at(x);
					}
				}
			}
		trim(line1);
		trim(line2);
		list.push_back(line1);
		list.push_back(line2);
		return list;
	}

	/*
	 * Returns file last modyfication time in string
	 */
	string getFileModyfication(string file) {
	   struct stat attrib;
	   stat(file.c_str(), &attrib);
	   char date[250];
	   strftime(date,250,"%a, %d %b %Y %X GMT", gmtime(&(attrib.st_ctime)));
	   return (string)date;
	}
}; /* namespace Time */
#endif /* QUERYRECDATA_H_ */
