// ============================================================================
// Name        : Small http server
// Author      : Rafal Lesniewski
// Version     :
// Copyright   : GNU
// Description : C++
// ============================================================================

#include <ting/net/Lib.hpp>
#include <ting/net/TCPSocket.hpp>
#include <ting/net/TCPServerSocket.hpp>
#include <ting/WaitSet.hpp>
#include <plugin/Debug.h>

#include <iostream>
#include <string>
#include <stdio.h>

#include "MyThread.h"
#include "ThList.h"

#define NMBTH 8
ting::net::TCPServerSocket ls;

using namespace std;

int main(int argc,char *argv[]) {
if(argc < 0 || argc == 1) {
	cout << "Bad params." << endl;
	return 0;
} else if(strcmp(argv[1],"--help")==0) {
	cout << "Usege: recone [port] [debug on/off]" << endl;
	cout << endl;
	cout << "Small RECONE HTTP server." << endl;
	cout << "reCone HTTP v1.0, (C)2019 by Rafal Lesniewski, recone.org" << endl;
	return 0;
}

cout << "SERVER START" << endl;
cout << "Port: " << atoi(argv[1]) << endl;

Debug::setShow(atoi(argv[2]));

try{
	ting::net::Lib lib;
	ting::net::TCPServerSocket listenSock;
	ting::net::TCPSocket sock;
	ting::WaitSet ws(1);

	listenSock.Open(atoi(argv[1]));

	ThList* lista = new ThList();
	lista->Start();
	ws.Add(listenSock, ting::Waitable::READ);
	while(true) {
		ws.Wait();
		if(listenSock.CanRead()){
			if(listenSock.IsValid()) {
				sock = listenSock.Accept();

				if(lista->to.size() < NMBTH) {
					MyThread* th = new MyThread(sock);
					th->Start();
					lista->addThr(th);
				} else {
					Debug::co() << " To many threads !"  <<  Debug::endl();
					while(lista->to.size() >= NMBTH) {
						sleep(1);
					}

					MyThread* th = new MyThread(sock);
					th->Start();
					lista->addThr(th);
				}
			  }
		} else {
			 Debug::co() << "Can not read thread ! "  <<  Debug::endl();
		}

	}
} catch(ting::net::Exc &e) {
 Debug::co() << "Network error: " << e.What() <<  Debug::endl();
}
return 0;
}
