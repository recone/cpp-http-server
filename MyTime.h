/*
 * Time.h
 *
 *  Created on: 9 Jan 2014
 *      Author: rafles01
 */

#ifndef MyTIME_H_
#define MyTIME_H_

#include <time.h>
#include <string>

using namespace std;
class MyTime {
public:
	/*
	 * Start it before using  object
	 */
	MyTime() {
		time(&startTime);
	}
	/*
	 * How meny seconds was spend from startTimer
	 */
	int upTime() {
		time_t actualtime;
		time(&actualtime);
		return difftime(actualtime,startTime);
	}
	/*
	 * Checks whether the 1s time was spend
	 */
	bool secondPass() {
		double actSec=0;
		actSec = upTime();
		if(actSec!=lastSec) {
			lastSec=actSec;
			return true;
		}
	}

	/*
	 * Get return actual time as a text, redable
	 */
	const string getAcTime() {
		time_t actualtime = 0;
		time(&actualtime);

		string buf(ctime(&actualtime));
		buf.replace(buf.length()-1,1,"");

		return buf;
	}
	time_t getAcTimeStamp() {
		time_t actualtime = 0;
		return time(&actualtime);
	}
	/*
	 * Return GMT Time
	 */
	const string getGMTime(time_t t) {
		char buffer[250];
		strftime(buffer,250,"%a, %d %b %Y %X GMT",localtime(&t));

		return buffer;
		}
private:
	time_t startTime; // czas uruchomienia funkcji startTimer()
	double lastSec;   // ile czasu uplynelo?  W sekundach
};

#endif /* MyTIME_H_ */
