/*
 * Debug.h
 *
 *  Created on: Nov 11, 2014
 *      Author: Rafal Lesniewski
 *      Uzycie: Debug::co() << " Pokaz ten tekst " << Debug::endl();
 */

#include <ting/net/TCPSocket.hpp>
#include <ting/net/TCPServerSocket.hpp>

#include <iostream>
#include <string>
#include "../MyTime.h"


#ifndef DEBUG_H_
#define DEBUG_H_

using namespace std;

class Debug {
private:
	static bool show;
	static string ip;
public:
	/**
	 * Get debuger state
	 * return true/false
	 */
	static bool getShow() {
		return Debug::show;
	}

	/*
	 * Do print debug msegs?
	 * return none
	 */
	static void setShow(bool sh) {
		Debug::show = sh;
	}
	static void setShow(int sh) {
		if(sh==0)
			Debug::show = false;
		else
			Debug::show = true;
	}
	Debug() {};
	static string endl() {
		return "\n";
	}
	static Debug co(ting::net::TCPSocket& sock) {
		if(Debug::show == true) {
			MyTime cp;
			cout << sock.GetRemoteAddress().host.ToString() << ", " << cp.getAcTime() << ": ";
		}
		Debug x;
		return x;
	}

	static Debug co() {
		if(Debug::show == true) {
			MyTime cp;
			cout << cp.getAcTime() << ": ";
		}
		Debug x;
		return x;
	}

    Debug& operator << (const char* os){
    	if(Debug::show == true) {
    		cout << os ;
    		cout << flush;
		}
    	return (*this);
    };
    Debug& operator << (const char& os){
    	if(Debug::show == true) {
    		cout << os ;
    		cout << flush;
		}
    	return (*this);
    };
    Debug& operator << (const float& os){
    	if(Debug::show == true) {
    		cout << os ;
    		cout<< flush;
		}
    	return (*this);
    };
    Debug& operator << (const int& os){
    	if(Debug::show == true) {
    		cout << os ;
    		cout << flush;
		}
    	return (*this);
    };
    Debug& operator << (const unsigned int& os){
    	if(Debug::show == true) {
    		cout << os ;
    		cout << flush;
		}
    	return (*this);
    };
    Debug& operator << (const string os){
    	if(Debug::show == true) {
    		cout << os ;
    		cout << flush;
		}
    	return (*this);
    };
};

#endif /* DEBUG_H_ */
