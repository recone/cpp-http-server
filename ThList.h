#ifndef THLIST_H_
#define THLIST_H_

#include <ting/mt/Thread.hpp>

#include "MyThread.h"

using namespace std;
using namespace ting::net;

class ThList : public ting::mt::Thread{
private:
public:
	vector<MyThread*>to;
    bool canWrite = false;

	ThList() throw(){}
	~ThList() throw(){}

    void lock() {
    	canWrite = true;
    }
    /**
     * Unlock
     */
    void unlock() {
    	canWrite = false;
    }
    /**
     * Waits till can write
     */
    bool waitUnlockLocked() {
    	while(canWrite) {
    		usleep(1);
    	}
    	return true;
    }

    /**
     * Add thread to list
     */
	void addThr(MyThread *th) {
		if(waitUnlockLocked()) {
			lock();
			to.push_back(th);
			unlock();
			}
	}

	/**
	 *  Looks for frozen threads, kills them and removes from list
	 */
	void findZombieThr(){
		if(waitUnlockLocked() && to.size()>0) {
			lock();
			for(vector<MyThread*>::iterator mi = to.begin(); mi != to.end(); mi++){
				if(!(*mi)->doCycle) {
					(*mi)->Join();
					delete *mi;

					to.erase(mi);
					break;
					}
			}
			unlock();
		}
	}
	/**
	 * Process main loop
	 */
	void Run(){
		while(true) {
			if(to.size()==0) {
				this->Sleep(10);
			} else {
				this->Sleep(2);
			}

			try {
				findZombieThr();
			}catch(exception &e) {
				std::cout << "Blad:" << e.what() << endl;
			}

		}
	}
};

#endif /* THLIST_H_ */
